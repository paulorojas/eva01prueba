<%-- 
    Document   : exit
    Created on : 04-abr-2020, 23:34:14
    Author     : LiL WOLF
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calculadora Interes Simple</title>
        <link rel="stylesheet" href=styleexit.css>
    </head>
    <body>
        <h1>Calculadora de Interes Simple</h1>
        
        <%
            float interes = (float) request.getAttribute("interes");
            float monto = (float) request.getAttribute("mon");
            float tasa = (float) request.getAttribute("tas");
            float anos = (float) request.getAttribute("an");
            float sumainteres =(float) request.getAttribute("sumainteres");
            %>
            
           
            <body>
        <h1>Calculadora de Interes Simple</h1>
        <div id="cal">     
    <form name="form" action="controlador" method="POST">
     
  <div class="form-group">
      
      <label for="Monto"><b>Monto Capital : </b> $ <%=monto%></b></label>
    
  </div>
  <div class="form-group">
      <label for="Tasa"><b>Tasa anual de : </b>  <%=tasa%> % </b></label>
    
  </div>
  <div class="form-group">
      <label for="Años"><b>Años Plazo : </b><%=anos%></label>
    
  </div>        
      
  <div class="form-group">
      <label for="interes"><b>Se Genera un Interes de : </b> $ <%=interes%></label>
    
  </div>     
      
  <div class="form-group">
      <label for="sumainteres"><b>Suma Monto + Intereses :</b> $ <%=sumainteres%></label>
    
  </div>  
          
        </form>
            </div>
    </body>
            
    </body>
</html>
