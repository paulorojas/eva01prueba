<%-- 
    Document   : index
    Created on : 04-abr-2020, 23:24:00
    Author     : LiL WOLF
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    
    <head>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calculadora Interes Simple</title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <h1>Calculadora de Interes Simple</h1>
        <div id="cal">     
    <form name="form" action="controlador" method="POST">
     
  <div class="form-group">
      
      <label for="Monto">Monto <b>*En Pesos</b></label>
    <input type="number" name="monto" class="form-control" id="Monto" placeholder="Ingresa Aqui" required>
  </div>
  <div class="form-group">
      <label for="Tasa">Tasa de Interes <b>(%)</b></label>
    <input type="number" name="tasa" step="any" class="form-control" id="Tasa" placeholder="Ingrese Aqui"required>
  </div>
  <div class="form-group">
      <label for="Años">Años</label>
    <input type="number" name="anos" class="form-control" id="Años" placeholder="Ingrese Aqui"required>
  </div>        
            
  <button type="submit" class="btn btn-primary">Calcular</button>
        </form>
            </div>
    </body>
</html>
